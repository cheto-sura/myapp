package com.master.myapp.vo.enumClass

enum class Language {
    TH,
    EN
}