package com.master.myapp.vo.enumClass

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}