package com.master.myapp.di.component

import android.app.Application
import com.master.myapp.ProjectApplication
import com.master.myapp.di.module.ActivityModule
import com.master.myapp.di.module.DataModule
import com.master.myapp.di.module.PreferenceModule
import com.master.myapp.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        PreferenceModule::class,
        ActivityModule::class,
        DataModule::class,
        ViewModelModule::class
    ])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(awesomeApplication: ProjectApplication)
}