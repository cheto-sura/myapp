package com.master.myapp.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.master.myapp.view.main.MainActivity
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    fun provideMainActivity(activity: MainActivity): FragmentActivity {
        return activity
    }
}