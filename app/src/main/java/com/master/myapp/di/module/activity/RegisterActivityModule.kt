package com.master.myapp.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.master.myapp.view.register.RegisterActivity
import dagger.Module
import dagger.Provides

@Module
class RegisterActivityModule {

    @Provides
    fun provideRegister(activity: RegisterActivity): FragmentActivity {
        return activity
    }
}