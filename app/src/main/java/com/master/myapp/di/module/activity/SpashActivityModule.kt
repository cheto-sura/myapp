package com.master.myapp.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.master.myapp.view.splashScreen.SplashScreenActivity
import dagger.Module
import dagger.Provides

@Module
class SpashActivityModule {

    @Provides
    fun provideSplashScreenActivity(activity: SplashScreenActivity): FragmentActivity {
        return activity
    }
}