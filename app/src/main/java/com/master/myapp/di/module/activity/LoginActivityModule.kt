package com.master.myapp.di.module.activity

import androidx.fragment.app.FragmentActivity
import com.master.myapp.view.login.LoginActivity
import dagger.Module
import dagger.Provides

@Module
class LoginActivityModule {

    @Provides
    fun provideLoginActivity(activity: LoginActivity): FragmentActivity {
        return activity
    }
}