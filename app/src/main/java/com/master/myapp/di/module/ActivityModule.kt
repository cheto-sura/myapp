package com.master.myapp.di.module

import com.master.myapp.di.module.activity.LoginActivityModule
import com.master.myapp.di.module.activity.MainActivityModule
import com.master.myapp.di.module.activity.RegisterActivityModule
import com.master.myapp.di.module.activity.SpashActivityModule
import com.master.myapp.di.module.fragment.FragmentModule
import com.master.myapp.view.login.LoginActivity
import com.master.myapp.view.main.MainActivity
import com.master.myapp.view.register.RegisterActivity
import com.master.myapp.view.splashScreen.SplashScreenActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [SpashActivityModule::class])
    abstract fun contributeSplashScreenActivity(): SplashScreenActivity

    @ContributesAndroidInjector(modules = [LoginActivityModule::class])
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [RegisterActivityModule::class])
    abstract fun contributeRegisterActivity(): RegisterActivity

    @ContributesAndroidInjector(modules = [MainActivityModule::class,(FragmentModule::class)])
    abstract fun contributeMainActivity(): MainActivity
}