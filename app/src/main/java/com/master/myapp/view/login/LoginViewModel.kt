package com.master.myapp.view.login

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.master.myapp.data.rest.repository.GeneralRepository
import com.master.myapp.utils.SingleLiveData
import com.master.myapp.utils.TextHelper
import com.master.myapp.utils.watcher.TextWatcherAdapter
import com.master.myapp.vo.Resource
import com.master.myapp.vo.model.body.BodyLogin
import com.master.myapp.vo.model.response.ResponseLogin
import javax.inject.Inject


class LoginViewModel @Inject
constructor(private val generalRepository: GeneralRepository) : ViewModel() {

    val etUserName = ObservableField<String>("gobank@gmail.com")

    val etPassWord = ObservableField<String>("password")

    val isStatusButtonClick = ObservableField<Boolean>(false)

    var mLoginCall = SingleLiveData<Void>()

    var mOnClickListener = SingleLiveData<String>()

    val onUserNameTextChanged = TextWatcherAdapter { s ->
        etUserName.set(s)
        checkEventButtonClick()
    }

    val onPasswordTextChanged = TextWatcherAdapter { s ->
        etPassWord.set(s)
        checkEventButtonClick()
    }

    fun onClickLogin(){
        mLoginCall.call()
    }

    fun onClickRegister(){
        mOnClickListener.value = "intentRegister"
    }

    val mResponseLogin : LiveData<Resource<ResponseLogin>> = Transformations.switchMap(mLoginCall) {
        generalRepository.onLogin(
            BodyLogin(
                etUserName.get()!!, etPassWord.get()!!
                , "7C57196B27D826A2165F382821CF37C57196B27D826A2165F382821CF3", "th"
            )
        )
    }


    fun checkEventButtonClick() {
        if (TextHelper.isNotEmptyStrings(etUserName.get()) && TextHelper.isNotEmptyStrings(etPassWord.get()))
            isStatusButtonClick.set(true)
        else
            isStatusButtonClick.set(false)
    }

}