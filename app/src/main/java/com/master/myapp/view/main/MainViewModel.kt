package com.master.myapp.view.main

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.master.myapp.data.rest.repository.GeneralRepository
import com.master.myapp.utils.SingleLiveData
import com.master.myapp.vo.Resource
import com.master.myapp.vo.model.response.ResponseOrderList
import javax.inject.Inject

class MainViewModel @Inject constructor(generalRepository: GeneralRepository) : ViewModel() {
    val mCurrentPage = ObservableField(1)

    val mLastPage = ObservableField(1)

    val mOrderBookingCall = SingleLiveData<Void>()
    val mResponseOrderBooking : LiveData<Resource<ResponseOrderList>> = Transformations.switchMap(mOrderBookingCall){
        generalRepository.getOrderList(mCurrentPage.get()!!)
    }

}
