package com.master.myapp.data

object Constants {
    // retrofit request timeout
    const val REQUEST_TIMEOUT = 30L

    // time splash screen
    const val mTimeLoadPage = 3000L

    // Permission
    const val ACTION_GET_CAMERA = 1
    const val ACTION_GET_GALLERY = 2
    const val REQUEST_PERMISSION_SETTING = 999

    // More Debug
    const val MODE_DEBUG = true
}