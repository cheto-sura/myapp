package com.master.myapp.utils.facebook

enum class FacebookGetType {
    LOGIN,
    REGISTER
}